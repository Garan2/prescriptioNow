2023-01-03:
    Started planning API endpoints, started creating GitLab issues

2023-01-04:
    Finished planning API endpoints, continued creating GitLab issues

2023-01-05:
    Finished the issues and created stories for each issues.

2023-01-06:
    Voted on points for stories and issues. Divided the features and stories. Finished the pgAdmin.

2023-01-09:
    Reviewed branch structures and gitlab process. Finalized database table schemas and updated project. Planned next steps.

2023-01-10:
    Got the databases and respective tables working. Everyone created branches.

2023-01-11:
    Researched FastAPI and went through tutorial

2023-01-12:
    Worked on FastAPI and researching 3rd party api.

2023-01-13:
    Finished API tutorial, started planning project branch.

2023-01-17:
    Delivery tables and poller

2023-01-18:
    Researching api documentation and planning for implementation.

2023-01-19:
    Made a HERE developer Routing API formula to create a link

2023-01-20:
    Created a test embedded in map. Tested/fixed poller.

2023-01-23:
    Review merge request, project refactor. Setup some data for deliveries.

2023-01-24:
    Worked through map issues on React.

2023-01-25:
    Worked on authorization, deployment, a unit test, worked on data conversion skills.
2023-01-26:
    Implemented backend data with front-end
2023-01-27:
    Put many small finishing touches on project as choas unfolded