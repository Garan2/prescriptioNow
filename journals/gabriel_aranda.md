## Journals
## January 4, 2023

We worked on:

* Completing API endpoints
* As well as Creating Git lab issues

## January 5, 2023

We worked on:

* Completing the Git lab issues and stories
* Finished the project setup, setting up the PostgreSQL database.

## January 6, 2023

We worked on:

* Assigned story points, and then divided them out amongst the team members.
* As a team we set up pgadmin.

## January 9, 2023

We worked on:

* Everyone began creating their own branches, in order to begin coding.
* Finalized database table schemas and updated the project to set up migrations file.
* Learned about SQL tables, in order to create the tables.

## January 10, 2023

We worked on:

* All team members implemented their own tables, that worked for the team structure.
* Members related each branch to a feature / story, in order to begin working on features.

## January 11, 2023

I worked on:

* Learning more about fastapi and how the endpoint structure is.
* Began creating baseline endpoints that I could alter down the line.

## January 12, 2023

I worked on:

* FastApi research, as we were trying to implement a 3rd party api.
* Began to discuss and organize how jacob and I could implement the 3rd party api.

## January 13, 2023

I worked on:

* 3rd party api research
* Continued to work on endpoints for deliveries.

## January 17, 2023

I worked on:

* Developing deliveries backend
* Continued to try to figure out how to implement 3rd party api, as services were being changed.

## January 18, 2023

* Began to work on the backends structure, in order to pull from 2 different microservices
* Worked with jacob to create plan on what we needed to do.

## January 19, 2023

* Reviewed merge requests submitted from the previous day and approved for a merge.
* Setup CapRover
* Configured the cI/CD for the caprover deployment.

## January 20, 2023

* Completed backend endpoints.
* Began creating functions to work with the data coming from a poller.

## January 23, 2023

* Our group decided to refactor our entire project from four microservices down to two microservices.
* Reconfigured our databases to reflect these changes and developments we have encountered since initial creation.
* Reviewed merge requests for the refactor.
* Worked on backend endpoints, after communicating with the team better about what fields / collumns of information will be needed.

## January 24, 2023

* Worked on the deliveries backend endpoints, in order to prepare the type of data we need for the frontend.
* Began working on endpoint authentication.
* Created a longitude and latitude function for front-end use.

## January 25, 2023


* Created a email notification function, that notifies a user of a delivery status, on endpoint use. Using a real email and customer email given.
* Continued to work on endpoints.

## January 26, 2023

* Completed features, to the extent possible and updated backend with authentication.
* On button clicked on the front end, update status of delivery field in prescriptions, through prescriptions endpoint logic. 
* Began to pull code from main and begin working on pipeline issues.
* Completed Unit test for endpoint.
## January 27, 2023

I worked on:
* Merging latest code to main, in preperation for deployment
* Working on Read.me's and completing finishing touches.
