## Journals
## January 4, 2023

We worked on:

* Completed API endpoints design in our notion. 
* Discussed the project some more and made changes to our MVP.

## January 5, 2023

We worked on:

* Finished writing the stories and issues in Gitlab with my team.
* Finished the project setup, setting up the PostgreSQL database.

## January 6, 2023

We worked on:

* Assigned points to the stories and then divided them out amongst my team members.
* Setup the pgAdmin and made sure everyone could access it.

## January 9, 2023

We worked on:

* Reviewed the branch structure the instructors wanted, as well as the gitlab processed that are expected.
* Finalized database table schemas and updated the project to work with these.
* Planned our next steps as a team moving forward.

## January 10, 2023

We worked on:

* Got the databases and their respective tables working, updated the migration code to make it more streamlined.
* Everyone created there first story branches so we could begin our first story.

## January 11, 2023

I worked on:

* Thought we could start with authentication and realized endpoints needed to be done so we all began working on those first. I will likely end up doing authorization mostly on my own.
* Began working on my own today on the User endpoints. Ran into trouble with my own knowledge on how to implement them. Began researching.

## January 12, 2023

I worked on:

* Continued working on User endpoints and found a groove that helped my workflow.
* Figured out how to use FastAPI so that was helpful and things started to click so I am working on writing the endpoints and connecting them to the database.

## January 13, 2023

I worked on:

* Finished the User backend.
* Didn't have time to tackle anything else due to unexpected lectures and social hack hour.

## January 17, 2023

I worked on:

* Finished the customer backend
* Cleaned up the user backend for code cleanliness

## January 18, 2023

* Worked on authentication and customer signup form frontend.
* Submitted a merge request for the customer microservice.

## January 19, 2023

* Reviewed merge requests submitted from the previous day and approved for a merge.
* Setup CapRover
* Configured the cI/CD for the caprover deployment.

## January 20, 2023

* Continued to work on getting the pipelines running.
* Continued setting up Authentication and users microservice

## January 23, 2023

* Our group decided to refactor our entire project from four microservices down to two microservices. 
* Reconfigured our databases to reflect these changes and developments we have encountered since initial creation.
* Reviewed merge requests for the refactor.
* Worked on customer endpoints

## January 24, 2023

* Worked on the customer microservice to finish the last two stories.
* Was able to configure the backend and the list view for the frontend.
* Worked on the detail frontend page for the customer service.
* user signup story completed and merged.

## January 25, 2023

* Setup backend authentication for all of my features
* Login authentication
* Authentication and file clean up

## January 25, 2023

* Login authentication
* authentication
* determining customer prescription details

## January 26, 2023

* Finish stories and updated frontend with authentication
* Made several merge requests and merges
* Finished tests

## January 27, 2023

* Deployment
* Final merges
* Finalize READMEs
