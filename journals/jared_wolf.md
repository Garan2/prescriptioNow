2023-01-27: Continue working on deployment, adding data to production database, preparing for presentation to the instructors

2023-01-26: Continue working on CI/CD and writing a unit test for deleting prescriptions

2023-01-25: Finished form for creating new prescriptions, work on deployment and authorization

2023-01-24: Reviewed merge requests, worked on form for creating new prescriptions, worked on resolving pipeline issues

2023-01-23: Reviewed merge requests, refactored project from 4 services down to 2 services

2023-01-20:Continued reviewing merge requests, worked on backend for creating new prescriptions, worked on deployment and Ci & CD pipeline

2023-01-19: Continued reviewing merge requests, tested API endpoints, worked on backend for creating new prescriptions, worked on CapRover, deployment, and Ci & CD pipeline

2023-01-18: Reviewed merge requests, worked with Grace to configure our CapRover instance and start configuring CI/CD

2023-01-17: Set up Postman with all of the API endpoints for the project and for openFDA, reviewed users and prescriptions code to plan

2023-01-13: Continued researching 3rd-party API for getting prescription drug description. Most information is super technical, but the "indications_and_usage" field from the openFDA Drug Labeling API looks promising.

2023-01-12: Continued working on MainPage.js.

2023-01-11: Started configuring React app and nav, integrated Bootstrap, created MainPagejs for homepage.

2023-01-10: Researched 3rd-party API for getting prescription drug description.

2023-01-09: Team meeting to finalize database design, creating migration files, start local project setup.

2023-01-05: Team meeting to finalize GitLab feature issues, start creating GitLab story issues, start project setup.

2023-01-04: Team planning meeting to plan the MVP features, create GitLab issues for features, and work on API endpoint design.
